//MUSS AUF JEDEN FALL VERBESSERT WERDEN

// First Grid
const pfeil_rechts = document.querySelector('.article_grid_1 .pfeil_rechts');
const pfeil_links = document.querySelector('.article_grid_1 .pfeil_links');
const items = document.querySelectorAll('.article_grid_1 .article_content');

let counter = 0;

pfeil_rechts.addEventListener('click', toggleRechts);
pfeil_links.addEventListener('click', toggleLinks);


function toggleRechts() {
    if(counter < items.length) {
        counter++;
        pfeilToggled();
    }

    if (counter > items.length - 2) {
        pfeil_rechts.classList.add('hide');
    } else {
        pfeil_links.classList.remove('hide');
    }
}

function toggleLinks() {
    if(counter > 0) {
        counter--;
        pfeilToggled();
    }
    if (counter < 1) {
        pfeil_links.classList.add('hide');
    } else {
        pfeil_rechts.classList.remove('hide');
    }
}

function pfeilToggled() {
    for (let index = 0; index < items.length; index++) {
        const element = items[index];
        if (index == counter) {
            element.classList.remove('hide');
            element.classList.add('show');
        } else {
            element.classList.remove('show');
            element.classList.add('hide');
        }
    }
}



// Second Grid
const pfeil_rechts2 = document.querySelector('.article_grid_2 .pfeil_rechts');
const pfeil_links2 = document.querySelector('.article_grid_2 .pfeil_links');
const items2 = document.querySelectorAll('.article_grid_2 .article_content');

let counter2 = 0;

pfeil_rechts2.addEventListener('click', toggleRechts2);
pfeil_links2.addEventListener('click', toggleLinks2);


function toggleRechts2() {
    if(counter2 < items2.length) {
        counter2++;
        pfeilToggled2();
    }

    if (counter2 > items2.length - 2) {
        pfeil_rechts2.classList.add('hide');
    } else {
        pfeil_links2.classList.remove('hide');
    }
}

function toggleLinks2() {
    if(counter2 > 0) {
        counter2--;
        pfeilToggled2();
    }
    if (counter2 < 1) {
        pfeil_links2.classList.add('hide');
    } else {
        pfeil_rechts2.classList.remove('hide');
    }
}

function pfeilToggled2() {
    for (let index = 0; index < items2.length; index++) {
        const element = items[index];
        if (index == counter) {
            element.classList.remove('hide');
            element.classList.add('show');
        } else {
            element.classList.remove('show');
            element.classList.add('hide');
        }
    }
}

