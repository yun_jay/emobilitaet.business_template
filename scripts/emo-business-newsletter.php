<?php

$email = $_POST['email'];
$email = htmlspecialchars( $email );
$email = htmlentities( $email );
$email = strip_tags( $email );

if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    
    $date = date("m.d.y H:m:s");

    $content = "Mail: " . $email . ", Timestamp: " . $date . ", Server: " . $_SERVER['REMOTE_ADDR'];
    
    if (!file_put_contents('../vendor/newsletter-list.txt', $content.PHP_EOL , FILE_APPEND | LOCK_EX)) {
        echo "";
    }
}
else {
    echo "";
}

header('Location: /lollipopshop');


?>