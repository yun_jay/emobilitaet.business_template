window.addEventListener('load', function () {
    // Select DOM Items
    const menuBtn = document.querySelector('.menu-btn');
    const menu = document.querySelector('.menu');
    const menuNav = document.querySelector('.nav');
    const navItem = document.querySelectorAll('.nav-item');

    //Set Initial State Of Menu
    let showMenu = false;

    menuBtn.addEventListener('click', togglemMenu);

    function togglemMenu() {
        if(!showMenu) {
            menuBtn.classList.add('close');
            menu.classList.add('show');
            menuNav.classList.add('show');
            navItem.forEach(item => item.classList.add('show'));

            // Set Menu State 
            showMenu = true;
        } else {
            menuBtn.classList.remove('close');
            menu.classList.remove('show');
            menuNav.classList.remove('show');
            navItem.forEach(item => item.classList.remove('show'));

            // Set Menu State 
            showMenu = false;
        }
    }
});