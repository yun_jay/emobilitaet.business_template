<?php defined('_JEXEC') or die('Restricted access');
$tpar = $this->params;


?>
<!DOCTYPE html>
<html xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
  
  <?php if ($tpar->get("favicon"))
  {
    ?>
    <link href="<?php echo $tpar->get("favicon"); ?>" rel="shortcut icon"> <?php
  } ?>
  <?php if ($tpar->get("apple_touch"))
  {
    ?>
    <link href="<?php echo $tpar->get("apple_touch"); ?>" rel="apple-touch-icon"> <?php
  } ?>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="format-detection" content="telephone=no">
  <meta name="google-site-verification" content="jDS1LV3elftiGrjeevKz99m1Y472RBr9x3AEFX941ZU" />

  <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css"/>

  <style>
    <?php echo $tpar->get("custom_css"); ?>
  </style>

  <!-- START2 -->
  <meta name="google-site-verification" content="rGcrGDZuVPiURBaPXYomOMvstmaQVHvs9SguEMRfvNU"/>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MKVHVNW');</script>
  <!-- End Google Tag Manager -->
  <!-- Global site tag (gtag.js) - Google Ads: 995218284 -->
  <script async
          src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/scripts/googletagmanager.js?id=AW-995218284"></script>
  <script> window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'AW-995218284'); </script>
  <!-- END2 -->
  <jdoc:include type="head" />
    
</head>
<body class="bg-img">
    <div class="allwrap">
      <header>
          <div class="wrapper-header">
              <jdoc:include type="modules" name="main_logo" />
              <?php if (!$this->countModules( 'main_logo' )) : ?>
              <span></span>
              <?php endif; ?>
            <span class="top-anchor"></span>
              <nav class="main_navigation">
                  <div class="menu-btn">
                      <div class="btn-line"></div>
                      <div class="btn-line"></div>
                      <div class="btn-line"></div>
                  </div>
                  <jdoc:include type="modules" name="main_menu" />
              </nav>
          </div>
      </header>
      
      <main>
        <jdoc:include type="message"/>
        <jdoc:include type="component" />
        <jdoc:include type="modules" name="main" />
      </main>
      
      <jdoc:include type="modules" name="footer" />

    </div>

    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/scripts/header.js" type="text/javascript"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/scripts/mod_grid.js" type="text/javascript"></script>
</body>
</html>